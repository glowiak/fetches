#!/usr/bin/env perl
package Perlfetch;

use FindBin qw( $RealBin );
use lib $RealBin;

use libfetch;
use conio;

my $username = $ENV{USER};
my $hostname = $ENV{HOST};
my $opsystem = GetOS();
my $shell = $ENV{SHELL};

my $osversion = `uname -r`;
my $osarch = `uname -m`;
my $freespace = `df -h`;

$freespace = (split(/\n/, $freespace))[1];

my @fs = split(/ /, $freespace);

$freespace = @fs[12];

$hostname = $ENV{HOSTNAME} if ($hostname == "");

if (index($opsystem, "BSD") != -1)
{
    my $ostype = "BSD";
} else
{
    my $ostype = "Linux";
}
clrscr();
gotoxy(0, 0);

if ($ostype == "BSD")
{
    textcolor($RED);
    PrintBSD();
    textcolor(0);
} else
{
    textcolor($YELLOW);
    PrintLinux();
    textcolor(0);
}

textcolor($RED);
gotoxy(35, 3);
print "${username}\@${hostname}";
gotoxy(35, 4);
for (1..length("${username}\@${hostname}"))
{
    print "-";
}

textcolor($GREEN);
gotoxy(35, 5);
print "OS: $opsystem";

gotoxy(35, 6);
print "Version: $osversion";

gotoxy(35, 7);
print "Architecture: $osarch";

textcolor($BLUE);
gotoxy(35, 8);
print "Shell: $shell";

gotoxy(35, 9);
print "Free space: $freespace";

textcolor($RED);
gotoxy(35, 10);
print "―――――";

textcolor($GREEN);
print "―――――";

textcolor($BLUE);
print "―――――";

textcolor(0);
gotoxy(0, 15);
