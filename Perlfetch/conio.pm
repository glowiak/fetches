package Perlfetch;

our $BLACK = 30;
our $RED = 31;
our $GREEN = 32;
our $YELLOW = 33;
our $BLUE = 34;
our $MAGENTA = 35;
our $CYAN = 36;
our $WHITE = 37;

my $currentColor = 0;

sub clrscr
{
    print "[2J";
}

sub cleanMarkup
{
    print "[0m";
}

sub blink
{
    print "[6m";
}

sub gotoxy
{
    my ($x, $y) = @_;
    print "[${y};${x}H";
}

sub textcolor
{
    my $color = @_[0];
    $currentColor = $color;
    print "\033[${color}m";
}

sub textbackground
{
    my $color = @_[0];
    
    if ($color == 0)
    {
        textcolor(0);
    } else
    {
        textcolor($color + 10);
    }
}

sub putch
{
    my $char = @_[0];
    my $x = @_[1];
    my $y = @_[2];
    
    gotoxy($x, $y);
    print $char;
}

sub cputs # basically the same as putch
{
    my $str = @_[0];
    my $x = @_[1];
    my $y = @_[2];
    
    gotoxy($x, $y);
    print $str;
}

1;
