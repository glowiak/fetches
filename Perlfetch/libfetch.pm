package Perlfetch;

# this is part of the perlfetch

sub GetOS
{
    open(DATA, "</etc/os-release");
    while (<DATA>)
    {
        my $good = $_;
        if (index($good, "NAME=") != -1 && index($good, "PRETTY_NAME") == -1)
        {
            $good =~ s/NAME=//;
            return $good;
        }
    }
}

sub PrintBSD
{
    print "   ooooooo
  `88P\"\"P88. doooooo
   88. od88' Y88PYPP',8888o
   88b`P888L  Y88.   d88'Y8b
   d8b   `Y8b   Y8L  d8P  88[
   d8b  _d88P __.88. 88[  Y88
   Y888888P' Y88888P 88'  d8P
    YP'''            YPood88'
                       `\"\"'\n";
}

sub PrintUNIX
{



    print ",.   do  doo.    o  d8,8o   ,o
88[  Y8b Y8888o d8b  ' Y88.d88'
88[  d8b d8b Y8888b d8p Y888P
88[  d8P d8b   Y88b d8b  888
Y88bd8P  Y8b    Y8) ]88 d888b.
 ``\"\"'    `      '   `'d8P `88.
                      d8P   Y8b
                       \"     `'\n";
}

sub PrintLinux
{
    print "_
  ,88
  ]88    ,oo    ,.
  ]88     Y88  d8P
  d8b      `88d8P
  d8b    ,oo8888.
  d8b   88P\"' `88.
  d8boo_i____  `88L
   YPP8888888P   \"'\n";
}

1;
